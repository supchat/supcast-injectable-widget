var popup;
var draggablePanel;
var brandingContainer;
var supcastButton;
var iFrame;

var infoPopup;
var statusTimeLabel;
var topicLabel;

var isWidgetOpened = false;
var isWidgetMinimized = false;
var isInfoOpened = false;
var isNarrowScreen;
var isVeryNarrowScreen;
var withBranding = false;

var backgroundColor = "#FFFFFF";
var textColor = "#222222";

const infoPopupWidth = 274;

const expandedNormalHeight = 336;
const expandedNarrowHeight = 410;
const expandedNormalWidth = 466;

const collapsedNormalFrameWidth = 462;
const collapsedNormalFrameHeight = 80;
const collapsedNarrowFrameHeight = 80;
const collapsedVeryNarrowFrameHeight = 130;

const draggableAreaWidth = 18;
const brandingLabelHight = 28;

const initialPopupPositonBottom = 140;
const initialPopupPositonRight = 40;

const draggableIcon = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAkCAYAAA" +
    "CwlKv7AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACw4AAAsOAUC+4UEAAABQ" + 
    "SURBVDhPY2xoaPjPAAX19fWMUCYYNDY2wuWYoDRBMKoQL0BRyMjIiIKRAXkm/v//HwUjAxp7Bh8gTy" + 
    "H1gwcfGFWIF6AoHFbJDB8Y/AoZGABhjR7Hk4qAXAAAAABJRU5ErkJggg==";
const closeIcon = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/" + 
    "9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxMAAAsTAQCanBgAAAIRSURB" +
    "VDhPfVLbShtRFJ09iXY0DTSON4oU+yZoIQ8pvoQ8Cf6CaLw8lnj5gIA/YYz08lBMLYgPPvgjRqcWQV" +
    "RUEBNvDzFxgi3jWmdO6gSxCzYz+7L2XufsI9lsdsrzvIyILPX2dm/097/7a7wMOTg4lOvr2zFw5sHJ" +
    "myQjMYzv14uL8uT9veuXPofU63UP5GnUfoFPzqzJyXCqsAgCyzs7v9KVyh0JQUi1WjO2t500anLwI7" +
    "AquDmTsvEzj0AN1o6C/N7efjqgRE12nN8TzMFvh9XAWbDtjnXZ2toUFnOyLojqgjk0//Hw8Aeybyib" +
    "k0muIDcbjw+tWZZlqAYIGpTNySj8rAspkcoEMR6Tstn40+DgwFo0+houko0GhFYyo6cpgko8NVyIxz" +
    "98t6xXfhQw9Vehrc0yKFtPVneirXHm1SCZaGoACM/Mr+82AXGmmiDBBnrP6sJ4ZjVZG7eTwxuYct06" +
    "XAUO8RoNXtozX9sc/xlDbrlYdNKoVWSYOsL/9rxq27FC8E5Yw1rX9d9JKJFImEdHJ7z5FfjcDfecwW" +
    "0Xenq6PDTxOjvtYqlUPkVuBMZ3MloqXZ6jyS4afBzX5MaeM9xzJEIhPsLhsBGLvXHK5aszuKMwJkew" +
    "9uNQMpn8Buc9TO95qBAka0hrawuUdOxyMnwq4cC+UCqV4rW+BXkRcn9SNvwg/l0YlVA2J8PtE5H8I2" + 
    "EPKm32L8nxAAAAAElFTkSuQmCC";

const braningUrl =
    "https://supcast.io/?utm_source=widget&utm_medium=branding&utm_campaign=referral";

var offset = { x: 0, y: 0 };
var bodyWidth = 0;
var bodyHeight = 0;
var widgetWidth = 0;
var widgetHeight = 0;

function initializeSupcast(clientId, homeRoute, theme) {
    const widgetUrl = "https://podcasts-d312e.web.app" +
        "?clientId=" + clientId + "&homeRoute=" + homeRoute + "&theme=" + theme;
    isNarrowScreen = window.matchMedia('(max-width: 536px)').matches;
    isVeryNarrowScreen = window.matchMedia('(max-width: 478px)').matches;
    backgroundColor = theme === "dark" ? "#2B2B2B" : "#FFFFFF";
    textColor = theme === "dark" ? "#FFFFFF" : "#222222";
    setupAnimations();
    buildInfoPopup();
    buildPopup(widgetUrl);
    buildButton();

    fetch("https://supcast.io/getWidgetAppearance", {
            headers: {
                "Content-Type": "application/json",
                "Client-Id": clientId
            }
        })
        .then(response => response.json())
        .then(data => {
            if (data.withBranding) {
                withBranding = true;
                recalculateFrameSize();
                brandingContainer.style.display = "block";
            }
        });
}

function setupAnimations() {
    var keyFrames = `
        @keyframes supcast-live-pulse {
            0% {
                transform: scale(0.95);
                box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.7);
            }
            70% {
                transform: scale(1);
                box-shadow: 0 0 0 10px rgba(0, 0, 0, 0);
            }
            100% {
                transform: scale(0.95);
                box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);
            }
        }

        @keyframes supcast-empty-to-upcoming {
            0% {
                transform: scale(1);
                box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);
            }
            50% {
                transform: scale(1.3);
                box-shadow: 0 0 0 8px rgba(0, 0, 0, 0.3);
            }
            100% {
                transform: scale(1);
                box-shadow: 0 0 8px 8px rgba(0, 0, 0, 0.1);
            }
        }

        @keyframes supcast-session-over {
            0% {
                transform: scale(1);
                box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);
            }
            50% {
                transform: scale(0.7);
                box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);
            }
            100% {
                transform: scale(1);
                box-shadow: 0 0 8px 8px rgba(0, 0, 0, 0.1);
            }
        }

        @keyframes supcast-fade-in {
            0% {
                opacity: 1;
            }
            100% {
                opacity: 0;
            }
        }

        @keyframes supcast-fade-out {
            0% {
                opacity: 0;
            }
            100% {
                opacity: 1;
            }
        }
    `;
    var style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = keyFrames;
    document.head.appendChild(style);
}

function buildPopup(url) {
    const widgetFrame = buildIFrame(url);
    const brandingLabel = buildBrandingLabel(textColor);
    draggablePanel = buildDraggablePanel();
    const widgetContainer = document.createElement("div");
    widgetContainer.style = `
        width: 100%; 
        height: 100%; 
        flex: 1;
    `;
    widgetContainer.appendChild(widgetFrame);
    widgetContainer.appendChild(brandingLabel);

    popup = document.createElement("div");
    recalculateFrameSize();
    popup.appendChild(draggablePanel);
    popup.appendChild(widgetContainer);

    document.body.appendChild(popup);
}

function buildInfoPopup() {
    infoPopup = document.createElement("div");
    recalculateInfoSize();

    const statusTimeBar = buildStatusTimeLabel();
    topicLabel = buildTopicLabel();
    infoPopup.appendChild(statusTimeBar);
    infoPopup.appendChild(topicLabel);

    document.body.appendChild(infoPopup);
}

function buildStatusTimeLabel() {
    const statusTimeBar = document.createElement("div");
    statusTimeBar.style = `
        display: flex;
        flex-direction: row;
    `;

    statusTimeLabel = document.createElement("div");
    const textNode = document.createTextNode("");
    statusTimeLabel.appendChild(textNode);
    statusTimeLabel.style = `
        color: ${textColor}; 
        font-weight: 500; 
        font-family: "Nunito Sans", sans-serif;
        text-decoration: none;
        margin: 0;
        flex: 1;
        padding-bottom: 8px;
    `;

    const closeButton = document.createElement('img');
    closeButton.src = closeIcon;
    closeButton.style = `
        width: 16px;
        height: 16px;
        padding 2px;
        cursor: pointer;
    `;

    closeButton.onclick = function(e) {
        infoPopup.style.display = "none";
        isInfoOpened = false;
    }

    statusTimeBar.appendChild(statusTimeLabel);
    statusTimeBar.appendChild(closeButton);

    return statusTimeBar;
}

function buildTopicLabel() {
    const topicLabel = document.createElement("div");
    const textNode = document.createTextNode("");
    topicLabel.appendChild(textNode);
    topicLabel.style = `
        color: ${textColor};
        font-family: "Nunito Sans", sans-serif;
        margin: 0;
        font-size: 24px;
        font-weight: 900;
        text-overflow: ellipsis;
        overflow: hidden;
        max-height: 66px;
        cursor: pointer;
        display: -webkit-box;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
    `;
    topicLabel.onclick = function(e) {
        openWidget();
    }
    return topicLabel;
}

function buildButton() {
    supcastButton = document.createElement("div");
    supcastButton.style = `
        background: #969696;
        border-radius: 50%;
        margin: 10px;
        height: 80px;
        width: 80px;
        position: fixed;
        right: 20px;
        bottom: 20px;
        cursor: pointer;
        box-shadow: 0 0 8px 8px rgba(0, 0, 0, 0.1);
        transform: scale(1);
    `;
    supcastButton.onclick = function(e) {
        openWidget();
    }

    document.body.appendChild(supcastButton);
}

function openWidget() {
    popup.style.display = "flex";
    infoPopup.style.display = "none";
    isInfoOpened = false;
    isWidgetOpened = true;
}

function buildIFrame(url) {
    iFrame = document.createElement("iframe");
    iFrame.src = url;
    iFrame.allow = "microphone";
    iFrame.style = `
        width: 100%; 
        height: 100%; 
        border: 0;
    `;
    return iFrame;
}

function buildBrandingLabel() {
    const brandLink = document.createElement("a");
    const linkText = document.createTextNode("Powered by SupCast");
    brandLink.appendChild(linkText);
    brandLink.href = braningUrl;
    brandLink.target = "_blank";
    brandLink.style = `
        color: ${textColor}; 
        font-weight: 500; 
        font-family: "Nunito Sans", sans-serif;
        text-decoration: none;
    `;

    brandingContainer = document.createElement("div");
    brandingContainer.style = `
        text-align: center; 
        font-size: 12px; 
        padding: 4px;
        line-height: 1;
        display: none;
    `;
    brandingContainer.appendChild(brandLink);
    return brandingContainer;
}

function buildDraggablePanel() {
    const panelContainer = document.createElement("div");

    const icon = document.createElement('img');
    icon.src = draggableIcon;
    icon.draggable = false;
    icon.style = `
        width: 10px;
        height: 36px;
        padding 2px; 
        display: block; 
        margin: auto; 
        padding-left: 8px;
        max-width: none;
        box-sizing: content-box;
    `;

    panelContainer.appendChild(icon);
    panelContainer.style = ` 
        height: 100%; 
        flex: 0; 
        draggable: false;
        cursor: move;
        display: none;
        touch-action: none;
    `;
    panelContainer.addEventListener("pointerdown", onDraggableAreaPressed, { passive: true });
    return panelContainer;
}

function recalculateFrameSize() {

    var frameWidth = 0;
    var frameHeight = 0;
    var widgetWidth = 0;
    var widgetHeight = 0;
    var borderRadius = 0;
    var borderStyle = "";
    var rightCorner = 0;
    var bottomCorner = 0;
    const popupDisplay = isWidgetOpened ? "flex" : "none";
    const draggableAreaDisplay = isWidgetMinimized ? "flex" : "none";

    if (isWidgetMinimized) {
        if (isVeryNarrowScreen) {
            widgetWidth = window.innerWidth;
            infoWidth = window.innerWidth;
            widgetHeight = collapsedVeryNarrowFrameHeight;
            frameWidth = window.innerWidth - draggableAreaWidth;
            frameHeight = collapsedVeryNarrowFrameHeight;
        } else if (isNarrowScreen) {
            widgetWidth = window.innerWidth;
            infoWidth = window.innerWidth;
            widgetHeight = collapsedNarrowFrameHeight;
            frameWidth = window.innerWidth - draggableAreaWidth;
            frameHeight = collapsedNarrowFrameHeight;
        } else {
            widgetWidth = collapsedNormalFrameWidth + draggableAreaWidth;
            infoWidth = infoPopupWidth;
            widgetHeight = collapsedNormalFrameHeight;
            frameWidth = collapsedNormalFrameWidth;
            frameHeight = collapsedNormalFrameHeight;
        }
    } else {
        if (isVeryNarrowScreen || isNarrowScreen) {
            widgetWidth = window.innerWidth;
            infoWidth = window.innerWidth;
            widgetHeight = expandedNarrowHeight + (withBranding ? brandingLabelHight : 0);
            frameWidth = window.innerWidth;
            frameHeight = expandedNarrowHeight;
        } else {
            widgetWidth = expandedNormalWidth;
            infoWidth = infoPopupWidth;
            widgetHeight = expandedNormalHeight + (withBranding ? brandingLabelHight : 0);
            frameWidth = expandedNormalWidth;
            frameHeight = expandedNormalHeight;
        }
    }

    if (isVeryNarrowScreen || isNarrowScreen) {
        borderRadius = 0;
        borderStyle = "0 -4px 15px #0000011f";
        rightCorner = 0;
        bottomCorner = 0;
    } else {
        borderRadius = 16;
        borderStyle = "0 4px 15px #0000011f";
        rightCorner = initialPopupPositonRight;
        bottomCorner = initialPopupPositonBottom;
    }

    popup.style = `
        background-color: ${backgroundColor}; 
        border-radius: ${borderRadius}px;
        box-shadow: ${borderStyle};
        width: ${widgetWidth}px; 
        height: ${widgetHeight}px; 
        right: ${rightCorner}px; 
        bottom: ${bottomCorner}px; 
        overflow: hidden; 
        position: fixed; 
        z-index: 9001;
        display: ${popupDisplay};
    `;

    iFrame.style.width = frameWidth + "px";
    iFrame.style.height = frameHeight + "px";
    draggablePanel.style.display = draggableAreaDisplay;
}

function recalculateInfoSize() {

    var infoWidth = 0;
    var borderRadius = 0;
    var borderStyle = "";
    var rightCorner = 0;
    var bottomCorner = 0;
    const infoDisplay = isInfoOpened ? "flex" : "none";

    if (isVeryNarrowScreen || isNarrowScreen) {
        borderRadius = 0;
        borderStyle = "0 -4px 15px #0000011f";
        rightCorner = 0;
        bottomCorner = 0;
        infoWidth = window.innerWidth - 32;
    } else {
        borderRadius = 16;
        borderStyle = "0 4px 15px #0000011f";
        rightCorner = initialPopupPositonRight;
        bottomCorner = initialPopupPositonBottom;
        infoWidth = infoPopupWidth;
    }

    infoPopup.style = `
        background-color: ${backgroundColor}; 
        border-radius: ${borderRadius}px;
        box-shadow: ${borderStyle};
        width: ${infoWidth}px;
        right: ${rightCorner}px; 
        bottom: ${bottomCorner}px; 
        overflow: hidden; 
        position: fixed; 
        z-index: 9000;
        display: ${infoDisplay};
        padding: 16px;
        flex-direction: column;
    `;
}

window.onresize = function() {
    if (isWidgetOpened || isInfoOpened) {
        isNarrowScreen = window.matchMedia('(max-width: 536px)').matches;
        isVeryNarrowScreen = window.matchMedia('(max-width: 478px)').matches;
        if (isWidgetOpened) {
            recalculateFrameSize();
        }
        if (isInfoOpened) {
            recalculateInfoSize();
        }
    }
}

window.addEventListener("message", function(event) {
    if (event.data === "supcast-minimizeWidget") {
        isWidgetMinimized = true;
        recalculateFrameSize();
    } else if (event.data === "supcast-expandWidget") {
        isWidgetMinimized = false;
        recalculateFrameSize();
        onDraggableAreaReleased();
    } else if (event.data === "supcast-closeWidget") {
        popup.style.display = "none";
        isWidgetOpened = false;
    } else if (event.data.type === 'new_status') {
        if (event.data.status === 'empty') {
            onSessionEmpty();
        } else if (event.data.status === 'upcoming') {
            onSessionUpcomming(event.data.topic, event.data.time);
        } else if (event.data.status === 'live') {
            onSessionLive(event.data.topic, event.data.time);
        }
    }
}, false);

function onSessionEmpty() {
    supcastButton.style.backgroundColor = "#ADADAD";
    supcastButton.style.animation = "supcast-session-over 2s";
    infoPopup.style.display = "none";
    isInfoOpened = false;
}

function onSessionUpcomming(topic, time) {
    supcastButton.style.backgroundColor = "#5892E8";
    supcastButton.style.animation = "supcast-empty-to-upcoming 2s";

    const startTime = Date.parse(time);
    const now = Date.now();
    const timeDiff = Math.floor((startTime - now) / 1000 / 60);

    if (!isWidgetOpened && timeDiff <= 60) {
        const diffLabel = startTime < now ? "soon" : "in " + timeDiff + " minutes"; 
        statusTimeLabel.textContent = "Live " + diffLabel;
        topicLabel.textContent = topic;
        infoPopup.style.display = "flex";
        isInfoOpened = true;
    }
} 

function onSessionLive(topic, time) {
    supcastButton.style.backgroundColor = "#FF5F3B";
    supcastButton.style.animation = "supcast-live-pulse 2s infinite";

    if (!isWidgetOpened) {
        statusTimeLabel.textContent = "🔴 Live now";
        topicLabel.textContent = topic;
        infoPopup.style.display = "flex";
        isInfoOpened = true;
    }
}

function onDraggableAreaPressed(event) {
    bodyWidth = window.innerWidth;
    bodyHeight = window.innerHeight;
    widgetWidth = popup.clientWidth;
    widgetHeight = popup.clientHeight;

    offset.x = event.clientX - popup.offsetLeft;
    offset.y = event.clientY - popup.offsetTop;
    popup.style.position = "absolute";

    if (!isNarrowScreen) {
        iFrame.style.animation = "supcast-fade-in 0.15s";
        setTimeout(function() { 
            iFrame.style.display = "none"; 
        }, 150);
        brandingContainer.style.opacity = "0";
    }
    window.addEventListener("pointermove", movePopup, { passive: true });
    window.addEventListener("pointerup", onDraggableAreaReleased, { passive: true });
    window.addEventListener("pointercancel", onDraggableAreaReleased, { passive: true });
}

function onDraggableAreaReleased() {
    if (!isNarrowScreen) {
        iFrame.style.animation = "supcast-fade-out 0.15s";
        setTimeout(function() { 
            iFrame.style.display = "block"; 
            brandingContainer.style.opacity = "1";
        }, 150);
    }
    window.removeEventListener("pointermove", movePopup);
    window.removeEventListener("pointerup", onDraggableAreaReleased);
    window.removeEventListener("pointercancel", onDraggableAreaReleased);
}

function movePopup(event) {
    var top = event.clientY - offset.y;
    var left = event.clientX - offset.x;

    var maxCorrectedTop = top > bodyHeight - widgetHeight ? bodyHeight - widgetHeight : top;
    var maxCorrectedLeft = left > bodyWidth - widgetWidth ? bodyWidth - widgetWidth : left;

    var finalTop = maxCorrectedTop < 0 ? 0 : maxCorrectedTop;
    var finalLeft = maxCorrectedLeft < 0 ? 0 : maxCorrectedLeft;

    popup.style.top = finalTop + "px";
    popup.style.left = finalLeft + "px";
}
